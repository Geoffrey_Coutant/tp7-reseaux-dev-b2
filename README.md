# TP7 DEV : Websockets et databases

## I. Websockets

### 1. First steps

***🌞 ws_i_1_server.py et ws_i_1_client.py***

- Voici le code pour le fichier [ws_i_1_server.py](ws_i_1_server.py)

- Voici le code pour le fichier [ws_i_1_client.py](ws_i_1_client.py)

### 2. Client JS***

***🌞 ws_i_2_client.js***

- Voici le code pour le fichier [ws_i_2_client.js](ws_i_2_client.js)

