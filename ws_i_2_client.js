const socket = new WebSocket("ws://localhost:5000");

socket.addEventListener('open', (event) => {
    console.log('Connected to server');
    const name =prompt('Whats yous name ?');
    socket.send(name);
    console.log(`>>> ${name}`);
    const string =prompt(`Hello ${name} Whats a message for me: `) ;
    socket.send(string);
    console.log(`>>> ${string}`);
});

socket.addEventListener('message', (event) => {
    const greeting = event.data;
    console.log(`<<< ${greeting}`);
    alert(greeting);
});

socket.addEventListener('error', (event) => {
    console.log('Error', event);
});
socket.addEventListener('close', (event) => {
    console.log('Disconnected from server', event);
});